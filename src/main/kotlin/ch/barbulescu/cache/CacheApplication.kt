package ch.barbulescu.cache

import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.Expiry
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.Cacheable
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.caffeine.CaffeineCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import kotlin.random.Random
import kotlin.time.Duration.Companion.seconds


@SpringBootApplication
class CacheApplication

fun main(args: Array<String>) {
    runApplication<CacheApplication>(*args)
}

@Component
class Starter(val myService: MyService) : ApplicationRunner {
    override fun run(args: ApplicationArguments?) {
        repeat(60) {
            val value = myService.getLuckyNumber()
            println("Lucky number is: $value")
            Thread.sleep(1000)
        }
    }
}

data class LuckyNumber(val value: Int, val expiryTime: Long)

@Component
class MyService {

    @Cacheable("numbers")
    fun getLuckyNumber(): LuckyNumber = LuckyNumber(
        value = Random.nextInt(0, 10_000),
        expiryTime = 10
    )
}

@Configuration
@EnableCaching
class MainConfig {

    @Bean
    fun caffeineConfig(): Caffeine<Any, Any> = Caffeine.newBuilder()
        .expireAfter(CustomExpiry())

    @Bean
    fun cacheManager(caffeine: Caffeine<Any, Any>): CacheManager {
        val caffeineCacheManager = CaffeineCacheManager()
        caffeineCacheManager.setCaffeine(caffeine)
        return caffeineCacheManager
    }
}

class CustomExpiry : Expiry<Any, Any> {
    override fun expireAfterCreate(key: Any?, luckyNumber: Any, currentTime: Long): Long =
        if (luckyNumber is LuckyNumber) {
            luckyNumber.expiryTime.seconds.inWholeNanoseconds
        } else {
            currentTime
        }

    override fun expireAfterUpdate(key: Any?, value: Any, currentTime: Long, currentDuration: Long): Long =
        currentDuration

    override fun expireAfterRead(key: Any?, value: Any, currentTime: Long, currentDuration: Long): Long {
        return currentDuration
    }
}